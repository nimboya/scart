<?php
/* Import Classes */
include_once("user.php");
/* General Shopping Cart
Author: Ewere Diagboya
Company: Wicee Solutions
Date/Time: 2014-09-08 9:52PM
Location: My Brother's Room

Description: Shopping Cart Functionalities
*/
class Cart extends useroperations {

	function AddtoCart($itemname, $qty, $price)
	{
		session_start();
		if(!isset($_SESSION['cartid']) && $_SESSION['cartid'] == '')
		{
			$_SESSION['cartid'] = date('YmdHis');
		}
		$total = $qty * $price;
		$tablename = "cart";
		$fields = array('cartid','itemname','qty', 'unitprice','total');
		$values = array($_SESSION['cartid'],$itemname, $qty, $price, $total);
		$response = $this->InsertOpt($tablename, $fields, $values);
		return $response; 
	}
	
	function DeleteFromCart($cartid, $id)
	{
		// Delete Item from Cart
		$response = $this->Delete("WHERE `cartid` = '$cartid' AND `id`='$id'", "cart");
		return $response;
	}

	function UpdateQty($cartid, $id, $qty, $price)
	{
		// Update Stock Qty
		$tablename = "cart";
		$newtotal = $qty * $price;
		$response = $this->UpdateDB("SET `qty`='$qty', `total`='$newtotal' WHERE `cartid`='$cartid' AND `id`='$id'", $tablename);
		return $response;
	}

	function Checkout($cartid,$totalamount,$userid,$status="pending") 
	{
		// Perform Checkout
		$tablename = "cart";
		$response = $this->UpdateDB("SET `checkout`='1' WHERE `cartid`='$cartid'", $tablename);
		$fields = array('cartid','totalamount','userid','status');
		$values = array($cartid,$totalamount,$userid,$status);
		$this->InsertOpt("cartsum", $fields, $values);
		return $response;
	}
	
	function JoinusertoCart($cartid, $userid)
	{
		// Add User to Cart
		$res = $this->dbconect();
		$tablename = "cart";
		
		// Get Details
		$getuserqry = mysqli_query($res, "SELECT id, firstname, lastname, phone, email FROM members WHERE email='$userid'");
		$user = mysqli_fetch_array($getuserqry);
		
		// Send Mail
		$subj = "Your order confirmation";
		$msg = '<img src="http://philhallmarks.com/themes/images/mainlogo24.png" alt="Phil-Hallmark Logo"  width="100" height="100" ><br/>';
		$msg .= "Dear ".$user['lastname'].",<br/>";
		$msg .= "Your order is succesful. Your order number is:".$cartid."<br/>";
		$msg .= " Your Total Price is : ".$totalamount."<br/>";
		$msg .= "For payment please Call: 08095957574";
		$msg .= "<p></p>";
		$msg .= "Choose from any of the following Courier Service <br>";
		$msg .= " DHL. Phone : 090.. <br>";
		$msg .= " CHASE. Phone : 090.. <br>";
		$msg .= " EDO Courier. Phone : 090.. <br>";
		$msg .= " Please note that you will be responsible for the service <br>";
		$msg .= "Thank you for using PhilHallmark Online";
		$msg .= "<p></p>";
		$msg .= "For Enquiries<br />";
		$msg .= "PhilHallmark<br />";
		$msg .= "Call: 08095957574<br />";
		$msg .= "Email: info@philhallmarks.com<br />";
		$this->mailnotifier($user['email'], $subj, $msg);
		
		// Send SMS
		$message = "Dear ".$user['lastname'].", Your order number is: ".$cartid.". and Your Total Price is : ".$totalamount." Thank you for using PhilHallmark Online";
		$send = file("http://smsc.xwireless.net/API/WebSMS/Http/v3.1/index.php?username=wicee&password=tarsus01&sender=Hallmark&to=".$user['phone']."&message=".urlencode($message)."&reqid=1&format=text");
		
		// Add User to Cart
		$response = $this->UpdateDB("SET `userid`='$userid', `checkout`='1'  WHERE `cartid`='$cartid'", $tablename);
		$_SESSION['cartid'] = NULL;
		return $response;
	}
	
	function CartData($cartid)
	{
		$getcartdata = "SELECT * FROM cart";
	}

}
?>